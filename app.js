const express = require('express');
const authRoutes = require('./routes/auth');
const mysql = require('mysql');
const dotenv = require('dotenv');
const port = 5502;


dotenv.config({ path: './.env'});

const app = express();


const db = mysql.createConnection({
   host: process.env.DATABASE_HOST,
   user: process.env.DATABASE_USER,
   password: process.env.DATABASE_PASSWORD,
   database: process.env.DATABASE,
	socketPath : process.env.DATABASE_SOCKETPATH
});

db.connect( (err) => {
   if (err) {
      console.log(err);
   } else {
      console.log('MYSQL Connected...');
   }
});

app.use('/api/auth', authRoutes);


app.listen(port, () => {
   console.log(`Server started on ${port}`);
});